SET GEN=.\tools\openapi-generator.jar
SET YAML=mymusic-api.yaml
SET GUID={313110AF-4E29-4834-9147-8E37BDC09319}
java -jar %GEN% generate -i %YAML% -g aspnetcore --additional-properties packageName=MyMusic.Api,packageGuid=%GUID%
