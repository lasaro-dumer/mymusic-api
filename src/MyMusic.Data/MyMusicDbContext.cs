﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MyMusic.Data.Entities;

namespace MyMusic.Data
{
    public class MyMusicDbContext : DbContext
    {
        private readonly IConfiguration _config;
        private readonly IHostingEnvironment _env;
        public string ConnectionString { get; }
        public bool InMemory { get { return string.IsNullOrEmpty(ConnectionString); } }


        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }

        public MyMusicDbContext(DbContextOptions options, IConfiguration configuration, IHostingEnvironment env)
         : base(options)
        {
            _config = configuration;
            _env = env;

            if (_env.IsDevelopment())
            {
                ConnectionString = _config["ConnectionStrings:DefaultConnection"];
            }
            else
            {
                ConnectionString = _config["ENV_DBCONN"];
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            if (InMemory)
            {
                optionsBuilder.UseInMemoryDatabase(databaseName: "MyMusicInMemoryData");
            }
            else
            {
                optionsBuilder.UseSqlServer(ConnectionString);
            }
        }
    }
}
