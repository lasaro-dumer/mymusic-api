﻿using System;

namespace MyMusic.Data.Entities
{
    public class Album
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public Artist Artist { get; set; }
    }
}
