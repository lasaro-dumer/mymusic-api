﻿using System.Collections.Generic;

namespace MyMusic.Data.Entities
{
    public class Artist
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<Album> Albums { get; set; }
    }
}
