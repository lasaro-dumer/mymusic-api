﻿using System.Threading.Tasks;

namespace MyMusic.Data.Repositories.Interfaces
{
    public interface IBaseRepository
    {
        bool InMemory { get; }
        // Basic DB Operations
        IBaseRepository Add<T>(T entity) where T : class;
        IBaseRepository Update<T>(T entity) where T : class;
        IBaseRepository Delete<T>(T entity) where T : class;
        Task<bool> SaveAllAsync();
    }
}
