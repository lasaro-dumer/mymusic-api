﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MyMusic.Data.Entities;
using MyMusic.Data.Repositories.Interfaces;

namespace MyMusic.Data.Repositories
{
    public class ArtistRepository : BaseRepository, IArtistRepository
    {
        public ArtistRepository(MyMusicDbContext context)
            : base(context)
        {

        }

        public IEnumerable<Artist> GetAllArtists()
        {
            return _context.Artists.ToList();
        }

        public Artist GetArtistById(long id)
        {
            return _context.Artists.Where(a => a.Id == id).FirstOrDefault();
        }

        public Artist GetArtistByIdWithAlbums(long id)
        {
            return _context.Artists.Where(a => a.Id == id).Include(a => a.Albums).FirstOrDefault();
        }

        public int GetArtistsCount()
        {
            return _context.Artists.Count();
        }
    }
}
