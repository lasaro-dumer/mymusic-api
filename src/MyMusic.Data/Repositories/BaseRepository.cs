﻿using System.Threading.Tasks;
using MyMusic.Data.Repositories.Interfaces;

namespace MyMusic.Data.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        protected MyMusicDbContext _context;

        public bool InMemory { get { return _context.InMemory; } }

        public BaseRepository(MyMusicDbContext context)
        {
            _context = context;
        }

        public IBaseRepository Add<T>(T entity) where T : class
        {
            _context.Add(entity);

            return this;
        }

        public IBaseRepository Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);

            return this;
        }

        public IBaseRepository Update<T>(T entity) where T : class
        {
            _context.Update(entity);

            return this;
        }

        public async Task<bool> SaveAllAsync()
        {
            return (await _context.SaveChangesAsync()) > 0;
        }
    }
}
