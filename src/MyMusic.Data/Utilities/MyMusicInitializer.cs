﻿using System;
using System.Threading.Tasks;
using MyMusic.Data.Entities;
using MyMusic.Data.Repositories.Interfaces;

namespace MyMusic.Data.Utilities
{
    public class MyMusicInitializer
    {
        private IArtistRepository ArtistRepository;

        public MyMusicInitializer(IArtistRepository artistRepository)
        {
            ArtistRepository = artistRepository;
        }

        public async Task<bool> Seed()
        {
            bool seeded = true;

            if (ArtistRepository.InMemory && ArtistRepository.GetArtistsCount() == 0)
            {
                Artist johnDoe = new Artist() { Name = "John Doe" };
                Artist johnMoe = new Artist() { Name = "John Moe" };
                Artist moeFoo = new Artist() { Name = "Moe Foo" };
                ArtistRepository
                    .Add(johnDoe)
                    .Add(johnMoe)
                    .Add(moeFoo);

                seeded = seeded && await ArtistRepository.SaveAllAsync();

                Album first = new Album() { Artist = johnDoe, Name = "The First Doe", ReleaseDate = DateTime.Parse("01/01/2018") };
                Album second = new Album() { Artist = johnDoe, Name = "The Second Doe", ReleaseDate = DateTime.Parse("02/01/2018") };

                ArtistRepository.Add(first);
                ArtistRepository.Add(second);

                seeded = seeded && await ArtistRepository.SaveAllAsync();
            }

            return seeded;
        }
    }
}
