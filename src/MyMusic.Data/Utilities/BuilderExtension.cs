﻿using Microsoft.Extensions.DependencyInjection;
using MyMusic.Data.Repositories;
using MyMusic.Data.Repositories.Interfaces;

namespace MyMusic.Data.Utilities
{
    public static class BuilderExtension
    {
        public static IServiceCollection AddMyMusicData(this IServiceCollection services)
        {
            services.AddDbContext<MyMusicDbContext>(ServiceLifetime.Scoped);
            services.AddScoped<IArtistRepository, ArtistRepository>();
            services.AddTransient<MyMusicInitializer>();

            return services;
        }
    }
}
