﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace MyMusic.Api.Utilities
{
    /// <summary>
    /// 
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Data.Entities.Album, Models.Album>()
                .ReverseMap();
            CreateMap<Data.Entities.Artist, Models.Artist>()
                .ReverseMap();
        }
    }
}
