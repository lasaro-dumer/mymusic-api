# MyMusic.Api - ASP.NET Core 2.0 Server

This is a sample server for a music library.

#### Cloud URL

The API can be accessed using https://mymusic-api.cfapps.io/

#### Run

Linux/OS X:

```
sh build.sh
```

Windows:

```
build.bat
```

## Run in Docker

```
cd src/MyMusic.Api
docker build -t mymusic.api .
docker run -p 5000:5000 mymusic.api
```
